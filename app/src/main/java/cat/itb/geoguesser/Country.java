package cat.itb.geoguesser;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

public class Country {

    private String name;
    private String capital;
    private int map;

    public Country(String name, String capital, int map) {
        this.name = name;
        this.capital = capital;
        this.map = map;
    }

    public String getName() {
        return name;
    }

    public String getCapital() {
        return capital;
    }

    public int getMap() {
        return map;
    }
}
