package cat.itb.geoguesser;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.Arrays;

public class GeoViewModel extends ViewModel {

    private int currentIndex = 0;
    private int points = 0;
    private ArrayList<Country> countriesQuestion = new ArrayList<Country>();
    private ArrayList<Country> countries = new ArrayList<Country>(
            Arrays.asList(new Country("Espanya", "Madrid",R.mipmap.espanya),
                    new Country("França", "París",R.mipmap.francia),
                    new Country("Itàlia", "Roma",R.mipmap.italia),
                    new Country("Polònia", "Varsòvia",R.mipmap.polonia),
                    new Country("Alemanya", "Berlin",R.mipmap.alemania),
                    new Country("Albània", "Tirana",R.mipmap.albania),
                    new Country("Andorra", "Andorra la Vella ",R.mipmap.andorra),
                    new Country("Àustria", "Viena",R.mipmap.austria),
                    new Country("Azerbaidjan", "Bakú",R.mipmap.azerbajan),
                    new Country("Bèlgica", "Brussel·les",R.mipmap.belgica),
                    new Country("Bielorússia", "Minsk",R.mipmap.belorrusia),
                    new Country("Bòsnia", "Sarajevo",R.mipmap.bosnia),
                    new Country("Bulgària", "Sofia",R.mipmap.bulgaria),
                    new Country("Dinamarca", "Copenhaguen",R.mipmap.dinamarca),
                    new Country("Ciutat del Vatica", "Ciutat del Vatica",R.mipmap.ciutat_vatica),
                    new Country("Eslovàquia", "Bratislava",R.mipmap.slovakia),
                    new Country("Eslovènia", "Ljubljana",R.mipmap.slovenia),
                    new Country("Finlàndia", "Hèlsinki",R.mipmap.finlandia),
                    new Country("Geòrgia", "Tbilisi",R.mipmap.georgia),
                    new Country("Regne Unit", "Londres",R.mipmap.reign_unit),
                    new Country("Grècia", "Atenes",R.mipmap.grecia),
                    new Country("Hongria", "Budapest",R.mipmap.hungria),
                    new Country("Irlanda", "Dublín",R.mipmap.irlanda),
                    new Country("Islàndia", "Reykjavík",R.mipmap.islandia),
                    new Country("Sèrbia", "Belgrad",R.mipmap.serbia),
                    new Country("Letònia", "Riga",R.mipmap.letonia),
                    new Country("Liechtenstein", "Vaduz",R.mipmap.liechtenstein),
                    new Country("Lituània", "Vílnius",R.mipmap.lituania),
                    new Country("Luxemburg", "Luxemburg",R.mipmap.luxemburgo),
                    new Country("Malta", "Valletta",R.mipmap.malta),
                    new Country("Moldàvia", "Chisinau",R.mipmap.moldavia),
                    new Country("Noruega", "Oslo",R.mipmap.noruega),
                    new Country("Països Baixos", "Amsterdam",R.mipmap.pais_baixos),
                    new Country("Portugal", "Lisboa",R.mipmap.portugal),
                    new Country("Romania", "Bucarest",R.mipmap.romania),
                    new Country("Rússia", "Moscou",R.mipmap.russia),
                    new Country("República Txeca", "Praga",R.mipmap.rep_checa),
                    new Country("San Marino", "San Marino",R.mipmap.sanmarino),
                    new Country("Suècia", "Estocolm",R.mipmap.suecia),
                    new Country("Suïssa", "Berna",R.mipmap.suissa),
                    new Country("Ucraïna", "Kíev",R.mipmap.ucraina)
            )
    );

    public void generateQuestions(){
        int randomQuestion;
        boolean continua = false;

        for(int i = 0; i<10; i++){
            randomQuestion = (int) (Math.random()*countries.size());
            countriesQuestion.add(countries.get(randomQuestion));

            continua = false;
        }
    }

    public boolean isFinish(){
        if(currentIndex >= countriesQuestion.size()-1){
            return true;
        }else {
            return false;
        }
    }

    public boolean isCorrect(String capital){
        return countriesQuestion.get(currentIndex).getCapital().equals(capital);
    }

    public String getCountry(){
        return countriesQuestion.get(currentIndex).getName();
    }

    public int getCurrentIndex(){
        return currentIndex;
    }

    public void moveToNext(){
        currentIndex++;
    }

    public String getCapital(){
        return countriesQuestion.get(currentIndex).getCapital();
    }

    public Country randomCountry(){
        int randomQuestion = (int) (Math.random()*countries.size());
        return countries.get(randomQuestion);
    }

    public int getMap(){
        return countriesQuestion.get(currentIndex).getMap();
    }

    public void onePointMore(){
        points++;
    }

    public int getPoints(){
        return points;
    }

}
