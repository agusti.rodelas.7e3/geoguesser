package cat.itb.geoguesser;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView question;
    Button option1, option2, option3, option4;
    ImageButton helpButton;
    SeekBar seekBar;
    ImageView imageView;
    GeoViewModel viewModel;
    AnimationDrawable thumbAnimation;
    AlertDialog.Builder builder;
    Animation animation;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        question = findViewById(R.id.question);
        option1 = findViewById(R.id.option1Button);
        option2 = findViewById(R.id.option2Button);
        option3 = findViewById(R.id.option3Button);
        option4 = findViewById(R.id.option4Button);
        helpButton = findViewById(R.id.helpButton);
        seekBar = findViewById(R.id.seekbar);
        imageView = findViewById(R.id.imageMap);

        viewModel = new ViewModelProvider(this).get(GeoViewModel.class);

        onStartScreen();

        option1.setOnClickListener(this);
        option2.setOnClickListener(this);
        option3.setOnClickListener(this);
        option4.setOnClickListener(this);
        helpButton.setOnClickListener(this);

        seekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.option1Button:
                if(viewModel.isCorrect(option1.getText().toString())) {
                    viewModel.onePointMore();
                }
                progressBarController();
                refreshScreen();
                break;
            case R.id.option2Button:
                if(viewModel.isCorrect(option2.getText().toString())) {
                    viewModel.onePointMore();
                }
                progressBarController();
                refreshScreen();
                break;
            case R.id.option3Button:
                if(viewModel.isCorrect(option3.getText().toString())){
                    viewModel.onePointMore();
                }
                progressBarController();
                refreshScreen();
                break;
            case R.id.option4Button:
                if(viewModel.isCorrect(option4.getText().toString())) {
                    viewModel.onePointMore();
                }
                progressBarController();
                refreshScreen();
                break;
            case R.id.helpButton:
                help();
                break;
        }
    }

    private void onStartScreen(){
        animation = AnimationUtils.loadAnimation(this, R.anim.country_animation);
        doAnimationFlagUp();
        seekBar.setProgress(viewModel.getCurrentIndex()+1);

        viewModel.generateQuestions();
        refreshScreen();
    }


    /**
     * Refresca la pantalla
     *  -suma un al index
     *  -mostra EL nom del país
     *  -mostra la imatge del país
     *  -genera les respostes
     *  -activa tots els butons per si s'haviesn desactivat
     */
    private void refreshScreen(){
        viewModel.moveToNext();
        win();
        question.setText(viewModel.getCountry());
        imageView.setImageResource(viewModel.getMap());
        imageView.setColorFilter(getResources().getColor(R.color.yellowMap));
        imageView.startAnimation(animation);
        generateAnswers();
        option1.setEnabled(true);
        option2.setEnabled(true);
        option3.setEnabled(true);
        option4.setEnabled(true);

    }

    private void win(){
        Toast.makeText(this, String.valueOf(viewModel.isFinish()), Toast.LENGTH_SHORT).show();
        if(viewModel.isFinish()){
            builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.title_alert_dialog);
            builder.setMessage("Has acabat amb una puntuacion de: "+viewModel.getPoints()+" sobre 10");
            builder.setNegativeButton(R.string.finish_alert_dialog, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            builder.setPositiveButton(R.string.restart_alert_dialog,new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent i = getBaseContext().getPackageManager()
                            .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
            });

            builder.show();
        }
    }

    /**
     * Deshabilita les respostes incorrectes
     * @return retorna true si ha deshabilitat una false si ja no es poden dehabilitar més
     */
    private boolean help(){
        ArrayList<Button> buttons = new ArrayList<Button>(
                Arrays.asList(option1,option2,option3,option4)

        );

        for(Button b: buttons){
            if(!viewModel.isCorrect(b.getText().toString()) && b.isEnabled()){
                b.setEnabled(false);
                return true;
            }

        }
        return false;
    }

    /**
     * Genera les respostes aleatoriament i les reparteix amb els cuatre botons
     * disponibles
     */
    private void generateAnswers(){
        ArrayList<String> answers = new ArrayList<String>();
        int correctAnswer = (int) (Math.random()*3);
        int randomAnswer, i = 0;

        while (i<4){
            String capital = viewModel.randomCountry().getCapital();
            if(!answers.contains(capital) && i != correctAnswer && !capital.equals(viewModel.getCapital())){
                answers.add(capital);
                i++;
            }else if(i == correctAnswer){
                answers.add(viewModel.getCapital());
                i++;
            }
        }

        option1.setText(answers.get(0));
        option2.setText(answers.get(1));
        option3.setText(answers.get(2));
        option4.setText(answers.get(3));
    }

    /**
     * Controlador del SeekBar
     */
    private void progressBarController(){
        doAnimationFlagDown();
        seekBar.setProgress(viewModel.getCurrentIndex()+1);
        doAnimationFlagUp();
    }


    /**
     * Fa l'animació del Thumb de la SeekBar
     */
    public void doAnimationFlagUp(){
        seekBar.setThumb(getResources().getDrawable(R.drawable.thumb_flag_anim1));
        thumbAnimation = (AnimationDrawable) seekBar.getThumb();
        thumbAnimation.start();
    }

    public void doAnimationFlagDown(){
        seekBar.setThumb(getResources().getDrawable(R.drawable.thumb_flag_anim2));
        thumbAnimation = (AnimationDrawable) seekBar.getThumb();
        thumbAnimation.start();
    }

}